import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 36,
    marginBottom: 16,
  },
  androidButtonText: {
    color: 'blue',
    fontSize: 20,
  },
  container: {
    flex: 1,
    paddingTop: 5,
  },
  item: {
    padding: 16,
    fontSize: 22,
  },
});
