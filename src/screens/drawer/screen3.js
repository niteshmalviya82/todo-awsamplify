import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
// import {FlatGrid} from 'react-native-super-grid';
// import {styles} from '../../styles/styles.js';

Screen3 = ({navigation}) => (
  <View style={styles.container}>
    <View style={styles.item}>
      <View>
        <Text>{'item1'}</Text>
      </View>
    </View>
    <View style={styles.item}>
      <View>
        <Text>{'item1'}</Text>
      </View>
    </View>
    <View style={styles.item}>
      <View>
        <Text>{'item1'}</Text>
      </View>
    </View>
    <View style={styles.item}>
      <View>
        <Text>{'item1'}</Text>
      </View>
    </View>
    <TouchableOpacity
      style={{
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 70,
        position: 'absolute',
        bottom: 50,
        right: 10,
        height: 70,
        backgroundColor: '#448aff',
        borderRadius: 100,
      }}
      onPress={() => navigation.navigate('Bottom Tabs')}>
      <Icon name="ios-add" size={30} color="#ffffff" />
    </TouchableOpacity>
  </View>
);

export default Screen3;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    alignItems: 'flex-start', // if you want to fill rows left to right
  },
  item: {
    width: '40%', // is 50% of container width
    margin: 10,
    backgroundColor: '#2196f3',
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
