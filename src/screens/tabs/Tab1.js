import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableOpacity,
  Button,
  Alert,
  TextInput,
  Platform,
  Keyboard,
} from 'react-native';
import {API, graphqlOperation} from 'aws-amplify';
import {listTodos} from '../../graphql/queries';
import {createTodo, deleteTodo} from '../../graphql/mutations';
import Icon from 'react-native-vector-icons/Ionicons';

import {styles} from '../../styles/styles.js';

const Tab1 = (props) => {
  const [toDoList, setToDoList] = useState([]);
  const [todoText, setTodoText] = useState('');
  useEffect(() => {
    handleListNotes();
  }, []);
  const handleListNotes = async () => {
    const {data} = await API.graphql(graphqlOperation(listTodos));
    console.log(data.listTodos.items);
    setToDoList(data.listTodos.items);
  };
  addTodo = async () => {
    if (todoText == '') {
      Alert.alert('Please enter TODO Text');
    } else {
      const payload = {name: todoText};
      const {data} = await API.graphql(
        graphqlOperation(createTodo, {input: payload}),
      );
      if (data) {
        const newNote = data.createTodo;
        const updatedNotes = [newNote, ...toDoList];
        setTodoText('');
        setToDoList(updatedNotes);
      }
    }
  };
  removeTodo = async (index, id) => {
    console.log(id);
    const payload = {id};
    const {data} = await API.graphql(
      graphqlOperation(deleteTodo, {input: payload}),
    );
    if (data) {
      setToDoList(
        toDoList.filter(function (value, ind, arr) {
          return value.id !== id;
        }),
      );
    }
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      style={{
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
      }}>
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            flexWrap: 'wrap',
            padding: 5,
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
          }}>
          <View style={{width: '80%', height: 10}}>
            <TextInput
              placeholder="Enter TODO Text"
              maxLength={50}
              height={30}
              caretHidden={false}
              placeholderTextColor="gray"
              underlineColorAndroid="transparent"
              onChangeText={(e) => {
                setTodoText(e);
              }}
              value={todoText}
            />
          </View>
          <View style={{width: '20%'}}>
            <Button title="Add" onPress={() => addTodo()} />
          </View>
        </View>
        <FlatList
          key="ss"
          data={toDoList}
          renderItem={({item, index}) => {
            const keyView = 'view' + index;
            const keyText = 'text' + index;
            const keyButton = 'button' + index;
            return (
              <View
                key={keyView}
                style={{
                  flex: 1,
                  padding: 5,
                  flexDirection: 'row',
                }}>
                <Text
                  style={{
                    flex: 1,
                    padding: 10,
                    fontSize: 18,
                  }}
                  key={keyText}>
                  {item.name}
                </Text>
                <Icon
                  name="ios-trash"
                  key={keyButton}
                  size={30}
                  color="#000000"
                  onPress={() => removeTodo(index, item.id)}
                />
              </View>
            );
          }}
        />
      </View>
    </KeyboardAvoidingView>
  );
};

export default Tab1;
